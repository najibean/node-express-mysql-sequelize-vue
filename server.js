const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const PORT = 8080

// perintah ini adalah untuk create table baru
const db = require('./app/models')
db.sequelize.sync()

// sedangkan ini, akan men-drop table seperti semula jika "force: true"
db.sequelize.sync({ force: false }).then(() => {
    console.log("Drop and re-sync db.");
  });

let corsOptions = {
    origin: 'http://localhost:8081'
}
app.use(cors(corsOptions))

// parse request of content-type - application/json
app.use(bodyParser.json())

// parse request of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}))

// simple route
app.get('/', function (req, res) {
    res.send({
        msg: 'Welcome to simple route!'
    })
})

// const router = require('./src/routes/tutorial')(app)
// app.use(router)

const router = require('./app/routes/tutorial')
app.use('/api/tutorials', router)

app.listen(PORT, function () {
    console.log('Server running on port ' + PORT)
})
