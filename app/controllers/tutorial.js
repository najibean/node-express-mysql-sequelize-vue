const db = require('../models')
const Tutorial = db.tutorials
const Op = db.Sequelize.Op

// Create and save a new tutorial
exports.create = function (req, res) {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({
            msg: 'Content can not be empty!'
        })
        return
    }

    // Create tutorial
    const tutorial = {
        title: req.body.title,
        description: req.body.description,
        published: req.body.published ? req.body.published : false
    }

    // Save tutorial in the database
    Tutorial.create(tutorial)
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || 'Some error occured while createing the Tutorial'
            })
        })
}

// Retrieve all tutorials from the database
exports.findAll = function (req, res) {
    const title = req.body.title
    let condition = title ? {
        title: {
            [Op.like]: `%${title}%`
        }
    } : null

    Tutorial.findAll({ where: condition })
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || 'Some error occured while retrieving tutorials'
            })
        })
}

// Find a single tutorial with an id
exports.findOne = function (req, res) {
    const id = req.params.id

    Tutorial.findByPk(id)
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || 'Error retrieving tutorial with id=' + id
            })
        })
}

// Update tutorial by the id in the request
exports.update = function (req, res) {
    const id = req.params.id

    Tutorial.update(req.body, {
        where: {
            id: id
        }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: 'Tutorial was updated succesfully'
                })
            } else {
                res.send({
                    message: 'Tutorial not found'
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || 'Error updating tutorial with id=' + id
            })
        })
}

// Delete a tutorial with the specified id in the request
exports.delete = function (req, res) {
    const id = req.params.id

    Tutorial.destroy({
        where: {
            id: id
        }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: 'Tutorial was deleted succesfully'
                })
            } else {
                res.send({
                    message: 'Tutorial not found'
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || 'could not delete Tutorial with id=' + id
            })
        })
}

// Delete all tutorials from the database
exports.deleteAll = function (req, res) {
    Tutorial.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({
                message: `${nums} Tutorials were deleted successfully`
            })
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || 'Some error occured while removing all tutorials'
            })
        })
}

// Find all publised tutorials
exports.findAllPublished = function (req, res) {
    // published: true --> di database adalah angka 1, dan 0 untuk false
    Tutorial.findAll({
        where: {
            published: true
        }
    })
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || 'Some error occured while retrieving tutorials'
            })
        })
}



