const tutorials = require('../controllers/tutorial')
const router = require('express').Router()

// Create a new tutorial
router.post('/', tutorials.create)

// Retrieve all tutorials
router.get('/', tutorials.findAll)

// Retrieve all published tutorials
router.get('/published', tutorials.findAllPublished)

// Retrieve a single tutorial with id
router.get('/:id', tutorials.findOne)

// Update a tutorial with id
router.put('/:id', tutorials.update)

// Delete all tutorials
router.delete('/all', tutorials.deleteAll)

// Delete a tutorial with id
router.delete('/:id', tutorials.delete)

/** 
 * PENEMPATAN POSISI SANGAT PENTING.
 * KUMPULKAN .GET DENGAN .GET & .POST DENGAN .POST & .PUT DENGAN .PUT & .DELETE DENGAN .DELETE
*/

module.exports = router
